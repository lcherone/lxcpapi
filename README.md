LXC Panel REST API PHP Client
------------------

**About:**

This php [composer package](https://packagist.org/packages/lcherone/lxcpapi) provides a simple object orientated way to interact with the [LXC Web Panel](https://github.com/claudyus/LXC-Web-Panel) v1 [REST API](https://github.com/claudyus/LXC-Web-Panel/blob/master/api.rst).

**Author:**

@lcherone


**Setup:**

Using composer add the package to your project:

   `composer require lcherone/lxcpapi`


**Initializing the client**

    <?php
    require 'vendor/autoload.php';

    /**
     * Initialize the API client class
     */
    $api = new lcherone\lxcpapi\APIClient([
        'base_uri'   => 'http://lxcpanel.example.com:5000',
        'api_key'    => 'api-key-generated-in-panel'
    ]);
    ?>


## Making calls: ##

To interact with the API its as simple as calling a class method or property, endpoints are dynamically loaded when traversed to.

### Containers ###

**Get**

    $containers = $api->containers->get();

**Get (container)**

    $container = $api->containers->get('container_name');

**Create**

    $container = $api->containers->create('my-container', 'sshd');

**Update**

    $container = $api->containers->update('my-container', 'start');

**Delete**

    $container = $api->containers->delete('my-container');

**Clone/Copy**

    $container = $api->containers->copy('my-container');

### Tokens ###

**Create**

    $tokens = $api->tokens->create('some-fancy-api-key', 'API Token');

**Delete**

    $tokens = $api->tokens->delete('some-fancy-api-key');