<?php
namespace lcherone\lxcpapi\endpoints {

    class tokens extends \lcherone\lxcpapi\service\baseEndpoint {

        use \lcherone\lxcpapi\service\http;
        use \lcherone\lxcpapi\service\response;

        public function __construct($config)
        {
            parent::__construct($config, __CLASS__);
        }

        public function create($name, $description = null)
        {
            $endpoint = $this->config->base_uri . '/api/v1/tokens';

            return $this->RequestPost($endpoint, json_encode([
                'token' => $name,
                'description' => $description
            ]));
        }

        public function delete($name)
        {
            $endpoint = $this->config->base_uri . '/api/v1/tokens';

            if (!empty($name)) {
                $endpoint .= '/' . $name;
            }

            return $this->RequestDelete($endpoint);
        }

    }

}