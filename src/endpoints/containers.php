<?php
namespace lcherone\lxcpapi\endpoints {

    class containers extends \lcherone\lxcpapi\service\baseEndpoint {

        use \lcherone\lxcpapi\service\http;
        use \lcherone\lxcpapi\service\response;

        public function __construct($config)
        {
            parent::__construct($config, __CLASS__);

            /* set endpoint address */
            $this->endpoint = $this->config->base_uri . '/api/v1/containers';
        }

        /**
         * Get container/s information
         *
         * Without optional name parameter will return all lxc containers on
         * the host and brief status information.
         *
         * Usage:  $result = $api->containers->get();
         * Result: Array
            (
                [0] => Array
                    (
                        [state] => running
                        [container] => ubuntu
                    )

                ...
            )
         *
         * With optional name parameter will return full information about the container.
         *
         * Usage:  $api->containers->get('ubuntu');
         * Result: Array
            (
                [blkio_use] => 39.61 GiB
                [cpu_use] => 7380.88 seconds
                [ip] => 10.0.3.125
                [kmem_use] => 0 bytes
                [link] => vethT23LAP
                [memory_use] => 355.35 MiB
                [name] => ubuntu
                [pid] => 375
                [rx_bytes] => 1.68 GiB
                [state] => RUNNING
                [total_bytes] => 3.43 GiB
                [tx_bytes] => 1.74 GiB
            )
         *
         * @param string $name [Optional] - Name of container
         * @return array
         */
        public function get($name = null)
        {
            $endpoint = $this->endpoint;

            if (!empty($name)) {
                $endpoint .= '/' . $name;
            }

            return json_decode(
                $this->RequestGet(
                    $endpoint . '?private_token=' . $this->config->api_key
                ),
                true
            );
        }

        /**
         * Copy or Clone a container - container must be stopped
         *
         * Usage:  $api->containers->copy('new_container_name', 'container_to_copy');
         *
         * @param string $name  - Name of new container
         * @param string $clone - Name of container toc clone
         * @return array
         */
        public function copy($name, $clone)
        {
            return $this->RequestPut($this->endpoint, json_encode([
                'name' => $name,
                'clone' => $clone
            ]));
        }

        /**
         * Create container
         *
         * Usage:  $api->containers->create('new_container_name', 'container_template', 'data_store type');
         *
         * @param string $name - Name of new container
         * @param string $template - Name of container template
         * @param string $store - Data store type
         * @param string $xargs - Conatiner build arguments
         * @return array
         */
        public function create($name, $template, $store = null, $xargs = null)
        {
            $data = [
                'name' => $name,
                'template' => $template
            ];

            if (!empty($store)) {
                $data['store'] = $store;
            }

            if (!empty($xargs)) {
                $data['xargs'] = $xargs;
            }

            return $this->RequestPut($this->endpoint, json_encode($data));
        }

        /**
         * Update container - stop, start, freeze
         *
         * Usage:  $api->containers->update('container_name', 'action');
         *
         * @param string $name - Name of container
         * @param string $action - Options are stop, start, freeze
         * @return array
         */
        public function update($name, $action)
        {
            $endpoint = $this->endpoint;

            if (!empty($name)) {
                $endpoint .= '/' . $name;
            }

            return $this->RequestPost($endpoint, json_encode([
                'action' => $action
            ]));
        }

        /**
         * Delete container
         *
         * Usage:  $api->containers->delete('container_name');
         *
         * @param string $name - Name of container
         * @return array
         */
        public function delete($name)
        {
            $endpoint = $this->endpoint;

            if (!empty($name)) {
                $endpoint .= '/' . $name;
            }

            return $this->RequestDelete($endpoint);
        }

    }

}
