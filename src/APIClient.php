<?php
namespace lcherone\lxcpapi {

	use \Exception;

	class APIClient {

		private $config;

		public function __construct($config = [])
		{
			$this->config = (object) $config;
		}

		public function __get($endpoint)
		{
			$class = __NAMESPACE__ . '\\endpoints\\' . $endpoint;

			if (class_exists($class)) {
				$this->config->endpoint = $endpoint;
				return $this->$endpoint = new $class($this->config);
			} else {
				throw new Exception(
					'Endpoint ' . $class . ', not implemented.'
				);
			}
		}
	}
}
