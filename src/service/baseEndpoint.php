<?php
namespace lcherone\lxcpapi\service {

    use \Exception;

    class baseEndpoint {

        public function __construct($config, $endpoint)
        {
            $this->config = $config;
            $this->endpoint = $endpoint;
        }
        
        public function __get($method)
        {
            $class = $this->endpoint.'\\'.$method;

			if (class_exists($class)) {
				$this->config->endpoint = $this->endpoint;
				$this->config->method = $method;
				return new $class($this->config);
			}

			if (method_exists($this, $method)) {
                return call_user_func(
                    array(
                        $this,
                        $method
                    ),
                    [$this->config]
                );
            } else {
                throw new Exception(
					'Endpoint '.$class.', not implemented.'
				);
            }
        }

    }

}
