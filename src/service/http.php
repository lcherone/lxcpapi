<?php
namespace lcherone\lxcpapi\service {

    use \Requests;

    trait http
    {
        public function RequestGet($url)
        {
            return Requests::get($url, [
                    'Private-Token' => $this->config->api_key,
                    'Content-Type' => 'application/json'
                ], [])->body;
        }

        public function RequestPut($url, $payload)
        {
            return Requests::put(
                $url, [
                    'Private-Token' => $this->config->api_key,
                    'Content-Type' => 'application/json'
                ], $payload, [])->body;
        }

        public function RequestPost($url, $payload)
        {
            return Requests::post(
                $url, [
                    'Private-Token' => $this->config->api_key,
                    'Content-Type' => 'application/json'
                ], $payload, [])->body;
        }

        public function RequestDelete($url)
        {
            return Requests::delete(
                $url, [
                    'Private-Token' => $this->config->api_key,
                    'Content-Type' => 'application/json'
                ], [])->body;
        }

    }
}
