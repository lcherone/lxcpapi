<?php
namespace lcherone\lxcpapi\service {

	use \Httpful\Request;

	trait response
	{
		public function pdf($body)
		{
			header('Content-Type: application/pdf');
			header('Content-Transfer-Encoding: binary');
			header('Connection: Keep-Alive');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . sprintf("%u", strlen($body)));

			exit($body);
		}

		public function json()
		{

		}

		public function xml()
		{

		}

		public function csv($body)
		{
			header('Content-Type: text/csv');
			header('Content-Transfer-Encoding: binary');
			header('Connection: Keep-Alive');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . sprintf("%u", strlen($body)));

			exit($body);
		}
	}
}
